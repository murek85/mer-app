# ** # NET.Core 3.1 / Angular9 / Fomantic UI / Covalent UI / ElectronJS ** #

1. NET.Core SDK
2. VSCODE


### Screenshots
![Login](/images/2020-05-20_10_46_47-MerApp.png)
![Dashboard](/images/2020-05-20_10_47_13-MerApp.png)
![Map](/images/2020-05-20_10_47_46-MerApp.png)
![Users](/images/2020-05-20_10_48_15-MerApp.png)
![Form](/images/2020-05-20_10_48_39-MerApp.png)
![Details](/images/2020-05-20_10_49_02-MerApp.png)
![Account](/images/2020-05-20_10_49_26-MerApp.png)

![Electron/Login](/images/2020-05-20_10_52_40-MedApp-Electron.png)
![Electron/Dashboard](/images/2020-05-20_10_53_52-MedApp-Electron.png)

### Components

* https://material.angular.io/components
* https://teradata.github.io/covalent/
* https://material.angular.io/components/
* https://materialdesignicons.com/
* https://www.materialui.co/colors/
* https://fomantic-ui.com/
