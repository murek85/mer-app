const path = require('path');
const json = require(path.resolve('./package.json'));
const exec = require('child_process').exec;
const execSync = require('child_process').execSync;
const fs = require('fs');

const spec = {
	'platform': {
		'win': 'win32'
	},
	'arch': {
		'32': 'ia32',
		'64': 'x64',
		'7l': 'armv7l',
		'arm': 'arm64'
	}
};

switch (process.env.NODE_OS) {
	case 'windows':
		if (fs.existsSync('./win_packager')) {
			console.log('Removing existing ./win_packager...');
			execSync("del /s /q win_packager && rmdir /s /q win_packager", { maxBuffer: 1024 * 2048 });
			console.log('Successfully removed ./win_packager/');
		}
		console.log('Creating windows packager...');
		exec('cross-env ng build --prod && electron-packager dist --overwrite --platform=' + spec['platform']['win'] + " --arch=" + spec['arch']['64'] + " --prune --asar --out=win_packager --icon=builder/icons/windows/favicon.ico", (error) => {
			if (!error) {
				console.log('Successfully created packager at ./win_packager/');
			} else {
				console.log('Error : ' + error);
			}
		});
		break;
	default:
		throw new Error('NODE_OS undefined');
}