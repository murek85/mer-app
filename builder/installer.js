var electronInstaller = require('electron-winstaller');

resultPromise = electronInstaller.createWindowsInstaller({
    appDirectory: './packagers/win_packager/mer_app-win32-x64',
    outputDirectory: './installers/win_installer',
    authors: 'Murek 2020',
    exe: 'mer_app.exe',
    name: 'mer_app',
    description: 'mer-app',
    version: '1.0.0',
    setupExe: 'mer_app_Setup',
    setupMsi: 'mer_app_Setup',
    title: 'mer-app Setup'
  });

resultPromise.then(() => console.log("Utworzono instalator aplikacji!"), (e) => console.log(`Wystąpił błąd: ${e.message}`));