import { Injectable } from '@angular/core';

import OlMap from 'ol/Map';
import OlXYZ from 'ol/source/XYZ';
import OlView from 'ol/View';
import OlTileLayer from 'ol/layer/Tile';
import Geolocation from 'ol/Geolocation';
import Point from 'ol/geom/Point';
import Feature from 'ol/Feature';
import TileWMS from 'ol/source/TileWMS';
import { Circle, Fill, Stroke, Style, Text, Icon, Polygon } from 'ol/style';
import { fromLonLat, toLonLat } from 'ol/proj';
import { defaults as defaultControls, ScaleLine } from 'ol/control.js';
import { Tile as TileLayer, Vector as VectorLayer, Heatmap as HeatmapLayer } from 'ol/layer';
import { Cluster as ClusterSource, OSM, Vector as VectorSource } from 'ol/source';
import { createForExtent } from 'ol/tilegrid';

import AnimatedCluster from 'ol-ext/layer/AnimatedCluster';

const SILESIA_LATITUDE = 50.264595;
const SILESIA_LONGITUDE = 19.025387;

@Injectable()
export class MapService {
  map: OlMap;
  geolocation;
  view;
  source;
  layer;
  clusterSource;
  clusterLayer;
  selectedCluster;
  selectedFeature;
  styleCache = {};

  constructor() { }

  private getBasicStyle() {
    return new Style({
      stroke: new Stroke({
        color: 'rgba(199,211,171,0.8)',
        width: 1
      }),
      fill: new Fill({
        color: 'rgba(199,211,171,0.1)'
      }),
    });
  }

  private getBasicStyleCluster() {
    return new Style({
      stroke: new Stroke({
        color: 'rgba(121,134,203,0.8)',
        width: 1
      }),
      fill: new Fill({
        color: 'rgba(121,134,203,0.6)'
      }),
    });
  }

  private getStyle(feature, resolution) {
    const features = feature.get('features');
    const measurement = features[0].get('measurement');
    console.log(features);
    const size = feature.get('features').length;
    const styleCache = {};
    let style = styleCache[size];
    if (!style) {
      const color = '244,67,54';

      const radius = Math.max(15, Math.min(size * 0.75, 20));
      style = styleCache[size] = [
        new Style({
          image: new Circle({
            radius: radius + 2,
            stroke: new Stroke({
              color: 'rgba(' + color + ',0.4)',
              width: 5
            })
          })
        }),
        new Style({
          image: new Circle({
            radius,
            fill: new Fill({
              color: 'rgba(' + color + ',0.7)'
            })
          }),
          text: new Text({
            text: '',
            fill: new Fill({
              color: '#000'
            })
          })
        }),
      ];
    }
    return style;
  }

  private getCenterOfExtent(extent) {
    const lon = extent[0] + (extent[2] - extent[0]) / 2;
    const lat = extent[1] + (extent[3] - extent[1]) / 2;
    return [lon, lat];
  }

  private setCenterAnimate(feature) {
    const size = this.map.getSize();

    const coordinates = feature.getGeometry().getCoordinates();
    const oldCenter = this.view.getCenter();
    this.view.centerOn(coordinates, size, [size[0] * 0.5, size[1] * 0.5]);
    const newCenter = this.view.getCenter();
    this.view.setCenter(oldCenter);
    this.view.animate({center: newCenter});
  }

  private registerEvents() {
    // klikanie na mapie
    this.map.on('click', e => { });
    this.map.on('postcompose', e => { });

    this.map.on('moveend', e => {
      const coordinates = toLonLat(e.map.getView().getCenter());
      const zoom = this.view.getZoom();
      // this.airlyService.getInstallations(coordinates[1], coordinates[0], 20, -1);
    });
  }

  private registerEventsMap() {
    this.registerEvents();
  }

  private fillMainLayer(layer) {
    const tileGrid = createForExtent([2007640, 6341965, 2223511, 6638902], 18, 512);

    switch (layer.type) {
      case 'xyz':
        this.source = new OlXYZ({
          urls: layer.url.split(',')
        });

        this.layer = new OlTileLayer({
          source: this.source,
          tileGrid
        });
        break;
      case 'tile':
        this.layer = new OlTileLayer({
          source: new TileWMS({
            url: layer.url,
            tileGrid
            // params: {FORMAT: layer.format, LAYERS: layer.layers, VERSION: layer.version},
          }),
        });
        break;
    }
  }

  private fillCluster() {
    this.clusterSource = new ClusterSource({
      distance: 1,
      threshold: 1,
      source: new VectorSource()
    });

    this.clusterLayer = new AnimatedCluster({
      name: 'cluster',
      source: this.clusterSource,
      animationDuration: 0,
      style: this.getStyle
    });
  }

  private addClusterFeatures() { }

  public generateMap() {
    this.view = new OlView({
      center: fromLonLat([SILESIA_LONGITUDE, SILESIA_LATITUDE]),
      zoom: 12
    });

    this.map = new OlMap({
      controls: [],
      renderer: (['webgl', 'canvas']),
      target: 'map',
      layers: [
        this.layer,
        this.clusterLayer,
      ],
      view: this.view,
      loadTilesWhileAnimating: true,
      loadTilesWhileInteracting: true
    });

    this.map.addControl(new ScaleLine());
  }

  public getMapData() {
    const mainwms = {
      url: 'https://a.tile.openstreetmap.org/{z}/{x}/{y}.png,https://b.tile.openstreetmap.org/{z}/{x}/{y}.png,https://c.tile.openstreetmap.org/{z}/{x}/{y}.png',
      type: 'xyz'
    };
    this.fillMainLayer(mainwms);
    this.fillCluster();
    this.generateMap();
    this.registerEventsMap();
    this.addClusterFeatures();

    this.clusterLayer.setVisible(true);
  }

  public changeMapZoomIn() {
    const view = this.map.getView();
    const zoom = view.getZoom() + 1;
    this.view.setZoom(zoom);
  }

  public changeMapZoomOut() {
    const view = this.map.getView();
    const zoom = view.getZoom() - 1;
    this.view.setZoom(zoom);
  }

  public changeMapZoom(value) {
    if (this.map) {
      this.map.getView().setZoom(value);
    }
  }

  public resetZoom() {
    const zoom = this.view.getZoom();

    this.view.animate({
      zoom: zoom - (zoom / 4),
      duration: 1000
    }, {
      center: fromLonLat([SILESIA_LONGITUDE, SILESIA_LATITUDE]),
      zoom: 9,
      duration: 1000
    });
  }
}
