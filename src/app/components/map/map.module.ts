import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { MapComponent } from './map.component';
import { MapRoutingModule } from './map-routing.module';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot()
];

@NgModule({
  declarations: [
    MapComponent
  ],
  imports: [
    MapRoutingModule,

    APP_MODULES
  ],
  providers: []
})
export class MapModule { }
