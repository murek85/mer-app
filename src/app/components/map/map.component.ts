import { Component, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { UserModel } from 'src/app/shared/models/user.model';

import { MapService } from './map.service';

declare var $: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  providers: [MapService]
})
export class MapComponent implements OnInit, AfterViewInit {

  userLogged: UserModel;

  sidenav = false;

  constructor(
    private router: Router,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private accountService: AccountService,
    private mapService: MapService) { }

  private initSemanticConfig() {
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);
    this.accountService.getUserLogged();

    this.mapService.getMapData();
  }
}
