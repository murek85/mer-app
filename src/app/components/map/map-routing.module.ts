import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapComponent } from './map.component';

import { AuthGuard } from 'src/app/core/services/auth.guard';

const routes: Routes = [
  { path: 'map',  component: MapComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class MapRoutingModule { }
