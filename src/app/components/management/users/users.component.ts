import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ViewChildren, QueryList, HostListener, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { TdLoadingService } from '@covalent/core/loading';
import { IPageChangeEvent } from '@covalent/core/paging';
import { TranslateService } from '@ngx-translate/core';

import { ScrollDispatcher, CdkScrollable } from '@angular/cdk/scrolling';

import { AppConfig } from 'src/app/app.config';

import { UsersService } from './users.service';

import { FilterObjectModel } from 'src/app/shared/models/filter.model';
import { SortObjectModel } from 'src/app/shared/models/sort.model';
import { PagingSettingsModel, PagingResultsModel } from 'src/app/shared/models/paging.model';

import { AccountService } from 'src/app/core/services/account.service';

import { UserModel } from 'src/app/shared/models/user.model';

import { FilterUserType } from 'src/app/shared/enums/filters/filter-user-type.enum';
import { FilterUserModel } from 'src/app/shared/models/filters/filter-user.model';

declare var $: any;

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, AfterViewInit {

  userLogged: UserModel;

  data: UserModel[];
  currentItem: UserModel;

  filters: FilterObjectModel<FilterUserType>[];
  sorts: SortObjectModel[];

  filterUserType: typeof FilterUserType = FilterUserType;

  pagingSettings: PagingSettingsModel = new PagingSettingsModel();
  pagingResults: PagingResultsModel = new PagingResultsModel();

  @ViewChildren('usersList') usersList: QueryList<any>;

  @ViewChild('header') headerRef: ElementRef;
  @ViewChild('filter') filterRef: ElementRef;
  @ViewChild('stats') statsRef: ElementRef;

  stats = [
    { name: 'wszystkich', type: 'all', icon: 'user friends', value: 0 },
    { name: 'zalogowanych', type: 'logged', icon: 'user', value: 0 },
    { name: 'nieaktywnych', type: 'deactivated', icon: 'user clock', value: 0 },
    { name: 'usuniętych', type: 'deleted', icon: 'user slash', value: 0 }
  ];

  date: Date = new Date();

  type: 'table' | 'list' = 'list';

  throttle = 50;
  scrollDistance = 2;

  constructor(
    public dialog: MatDialog,
    private scrollDispatcher: ScrollDispatcher,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private accountService: AccountService,
    private usersService: UsersService) {

    this.pagingSettings.pageNumber = 1;
    this.pagingSettings.pageSize = 5;

    this.data = [];
    this.filters = [];
    this.sorts = [];
  }

  private initSemanticConfig() { }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);
    this.accountService.getUserLogged();

    this.usersService.pagingResults.subscribe(pagingResults => {
      this.pagingResults = pagingResults; this.stats.find(x => x.type === 'all').value = pagingResults.totalRows;
    });
    this.usersService.users.subscribe(users => {
      this.data = this.type === 'table' ? users : users; this.currentItem = this.data[0];
    });

    this.scrollDispatcher.scrolled().subscribe((elementCdk: CdkScrollable) => {
      const elementRef = elementCdk.getElementRef();
      if (elementRef.nativeElement.scrollTop >= 30) {
        // this.headerRef.nativeElement.classList.add('masthead-header-fixed');
        // this.filterRef.nativeElement.classList.add('masthead-filter-fixed');
        // this.statsRef.nativeElement.classList.add('masthead-stats-fixed');
      } else {
        // this.headerRef.nativeElement.classList.remove('masthead-header-fixed');
        // this.filterRef.nativeElement.classList.remove('masthead-filter-fixed');
        // this.statsRef.nativeElement.classList.remove('masthead-stats-fixed');
      }
    });
  }

  onSelectedItem(item: UserModel): void {
    this.currentItem = item;
  }

  onRefreshData(): void {
    const filters: FilterUserModel = new FilterUserModel();

    // filtr konta niepotwierdzone
    const filtersObj: FilterObjectModel<FilterUserType>[] =
      this.filters.filter(x => x.type === FilterUserType.OnlyUserEmailConfirmed).map(x => {
        return x.value;
    });

    if (filtersObj.length > 0) {
      filters.onlyUserEmailConfirmed = Boolean(filtersObj[0]);
    }

    this.usersService.getUsers(this.pagingSettings, filters);
  }

  onPage(pagingEvent: IPageChangeEvent): void {
    this.pagingSettings.pageNumber = pagingEvent.page;
    this.pagingSettings.pageSize = pagingEvent.pageSize;
    this.onRefreshData();
  }

  onAcceptFilter(event: any) {
    this.pagingSettings.pageNumber = 1;
    this.onRefreshData();
  }

  onClearFilter(event: any) {
    $('.checkbox-only-user-email-confirmed').checkbox('uncheck');

    this.pagingSettings.pageNumber = 1;
    this.onRefreshData();
  }

  onChangeSortOrder(event: any) {
    this.pagingSettings.pageNumber = 1;
    this.onRefreshData();
  }

  onScrollDown(ev) {
  }

  onScrollUp(ev) {
  }

  private createFilter(
    name: string,
    type: FilterUserType,
    field: string,
    value?: any | null) {

    const filter: FilterObjectModel<FilterUserType> = new FilterObjectModel();
    filter.name = name;
    filter.type = type;
    filter.field = field;
    filter.value = value;
    filter.objectsModel = [];

    return filter;
  }

  private createSort(
    name: string,
    field: string,
    order?: string | null) {

    const sort: SortObjectModel = new SortObjectModel();
    sort.name = name;
    sort.field = field;
    sort.order = order;

    return sort;
  }

  @HostListener('window:scroll', ['$event'])
  scrollHandler(event): void {
    if (event.target.scrollTop >= 30) {
      this.headerRef.nativeElement.classList.add('masthead-header-fixed');
      this.filterRef.nativeElement.classList.add('masthead-filter-fixed');
    } else {
      this.headerRef.nativeElement.classList.remove('masthead-header-fixed');
      this.filterRef.nativeElement.classList.remove('masthead-filter-fixed');
    }
  }
}
