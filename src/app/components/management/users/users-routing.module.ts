import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersComponent } from './users.component';
import { UserComponent } from './details/user.component';
import { UserFormComponent } from './form/form.component';

import { AuthGuard } from 'src/app/core/services/auth.guard';

const routes: Routes = [
  {
    path: 'management', children: [
      {
        path: 'users', children: [
          { path: '', redirectTo: 'list', pathMatch: 'full' },
          { path: 'list', component: UsersComponent, canActivate: [AuthGuard] },
          { path: 'form', children: [
            { path: '', component: UserFormComponent, canActivate: [AuthGuard] },
            { path: ':id', component: UserFormComponent, canActivate: [AuthGuard] }
          ] },
          { path: ':id', component: UserComponent, canActivate: [AuthGuard] }
        ]
      }
    ]
  }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class UsersRoutingModule { }
