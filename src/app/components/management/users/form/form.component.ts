import { Component, Input, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CdkStepper } from '@angular/cdk/stepper';
import { TranslateService } from '@ngx-translate/core';
import { TdLoadingService } from '@covalent/core/loading';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';
import { UserModel } from 'src/app/shared/models/user.model';
import { RoleModel } from 'src/app/shared/models/role.model';
import { PermissionModel } from 'src/app/shared/models/permission.model';
import { GenderModel } from 'src/app/shared/models/gender.model';

import { UsersService } from '../users.service';

import * as moment from 'moment';
import { iif } from 'rxjs';
import { finalize } from 'rxjs/operators';

declare var $: any;

const calendarFormatter = {
  date: (date, settings) => {
    if (!date) {
        return '';
    }

    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    return day + '/' + month + '/' + year;
  }
};
const calendarText = {
  days: ['Nd', 'Pn', 'Wt', 'Śr', 'Czw', 'Pi', 'So'],
  months: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec',
    'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
  monthsShort: ['Sty', 'Lut', 'Mar', 'Kwi', 'Maj', 'Cze',
    'Lip', 'Sie', 'Wrz', 'Paź', 'Lis', 'Gru'],
  today: 'dzisiaj',
  now: 'teraz',
  am: 'am',
  pm: 'pm'
};

@Component({
  selector: 'app-user-form',
  styleUrls: ['./form.component.scss'],
  templateUrl: './form.component.html'
})
export class UserFormComponent implements OnInit, AfterViewInit {

  genders: GenderModel[];
  roles: RoleModel[];
  permissions: PermissionModel[];
  user: UserModel = { };
  steps: any[];
  selectedIndexStep = 0;
  accountAccess = false;

  accountType: 'local' | 'domain' = 'local';

  aspId = null;

  configHeader = {
    title: 'Tworzenie konta'
  };

  error: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private translateService: TranslateService,
    private tdLoadingService: TdLoadingService,
    private accountService: AccountService,
    private userService: UsersService,
    private sidebarService: SidebarService) {

    this.aspId = this.route.snapshot.params['id'];

    this.roles = [];
    this.steps = [];
  }

  public ngOnInit() {

    if (this.aspId) {
      this.userService.user.subscribe(user => this.user = user);
      this.userService.getUser(this.aspId);

      this.configHeader = {
        title: 'Edytowanie konta'
      };
    }

    // ustawienie przycisku 'BACK'
    this.sidebarService.setSidebarMenuBack.next({
      show: true,
      routerLink: `/management/users`,
    });

    this.initSteps();
    this.initGenders();
    this.initRoles();
    this.initPermissions();
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public changeStep($event) {
    const stepper = $event as CdkStepper;
    switch (stepper.selectedIndex) {
      case 0: {
        break;
      }
      case 1: {
        this.initSemanticLdap();
        this.initSemanticAccount();
        this.initSemanticGenders();
        break;
      }
      case 2: {
        break;
      }
      case 3: {
        this.initSemanticRoles();
        this.initSemanticPermissions();
        break;
      }
      case 4: {
        this.initSemanticAccountRestrictions();
        break;
      }
    }
  }

  private initSemanticConfig() {

    setTimeout(_ => {
      $('.checkbox').checkbox({
        onChecked: () => {
        },
        onUnchecked: () => {
        },
        onChange: () => {
          const $listGroup = $('.type-account-list');
          const $checkbox = $listGroup.find('.checkbox');

          $checkbox.each(index => {
          });
        }
      });

      $('.checkbox-type-account-local').checkbox({
        onChecked: () => {
          $('.checkbox-type-account-domain').checkbox('uncheck'); this.accountType = 'local';
        },
        onUnchecked: () => {
          $('.checkbox-type-account-domain').checkbox('check'); this.accountType = 'domain';
        }
      });

      $('.checkbox-type-account-domain').checkbox({
        onChecked: () => {
          $('.checkbox-type-account-local').checkbox('uncheck'); this.accountType = 'domain';
        },
        onUnchecked: () => {
          $('.checkbox-type-account-local').checkbox('check'); this.accountType = 'local';
        }
      });

      $('.checkbox-type-account-local').checkbox('check');
    }, 0);

    // initialize stepper
    this.initSemanticLdap();
    this.initSemanticAccount();
    this.initSemanticGenders();
  }

  private initSemanticAccountRestrictions() {
    setTimeout(_ => {
      $('.checkbox-account-email-confirmed').checkbox({
        onChecked: () => {
        },
        onUnchecked: () => {
        }
      });

      $('.checkbox-account-blocked').checkbox({
        onChecked: () => {
        },
        onUnchecked: () => {
        }
      });

      $('.checkbox-account-active').checkbox({
        onChecked: () => {
        },
        onUnchecked: () => {
        }
      });

      $('.checkbox-account-access').checkbox({
        onChecked: () => {
          this.accountAccess = !this.accountAccess;

          const date = new Date();
          $('.calendar-access-lowerbound').calendar('set date', moment(date).format('YYYY-MM-DD HH:mm:ss'));
          $('.calendar-access-upperbound').calendar('set date', moment(date.setDate(date.getDate() + 30)).format('YYYY-MM-DD 23:59:59'));
        },
        onUnchecked: () => {
          this.accountAccess = !this.accountAccess;

          $('.calendar-access-lowerbound').calendar('clear');
          $('.calendar-access-upperbound').calendar('clear');
        }
      });

      $('.calendar-access-lowerbound').calendar({
        formatter: calendarFormatter,
        text: calendarText,
        type: 'date',
        ampm: false,
        firstDayOfWeek: 1,
        endCalendar: $('#rangeend'),
        onChange: (value) => { }
      });

      $('.calendar-access-upperbound').calendar({
        formatter: calendarFormatter,
        text: calendarText,
        type: 'date',
        ampm: false,
        firstDayOfWeek: 1,
        startCalendar: $('#rangestart'),
        onChange: (value) => { }
      });
    }, 0);
  }

  private initSemanticAccount() {
    $('.calendar-birthday').calendar({
      formatter: calendarFormatter,
      text: calendarText,
      type: 'date',
      ampm: false,
      firstDayOfWeek: 1
    });
  }

  private initSemanticGenders() {
    setTimeout(_ => {
      $('.dropdown-genders').dropdown({ });
    }, 0);
  }

  private initSemanticLdap() {
    setTimeout(_ => {
      $('.dropdown-ldap').dropdown({ });
    }, 0);
  }

  private initSemanticRoles() {
    setTimeout(_ => {
      $('.dropdown-roles').dropdown({ });
    }, 0);
  }

  private initSemanticPermissions() {
    setTimeout(_ => {
      $('.dropdown-permissions').dropdown({ });
    }, 0);
  }

  public back() {
    this.router.navigate(['/management/users/list']);
    this.sidebarService.setSidebarMenuBack.next({ show: false, routerLink: null, queryParams: null });
  }

  public confirmForm() {
    this.tdLoadingService.register('listSyntax');
    iif(() => !this.aspId,
      this.userService.createUser(this.user),
      this.userService.updateUser(this.user))
        .pipe(finalize(() => this.tdLoadingService.resolve('listSyntax')))
        .subscribe(
          value => this.back(),
          err => this.error = err.error,
          () => { }
        );
  }

  private initSteps() {
    const steps = [];
    steps.push(this.createStep('Typ konta', 'Wskazanie rodzaju typu konta za pomocą którego użytkownik będzie logował się w aplikacji.'));
    steps.push(this.createStep('Konto użytkownika', 'Wypełnienie podstawowych informacji o koncie użytkownika umożliwiających jego identyfikację.'));
    steps.push(this.createStep('Hasło użytkownika', 'Wygenerowanie domyślnego hasła dostępu do tworzonego konta użytkownika.'));
    steps.push(this.createStep('Role i uprawnienia', 'Wybranie roli i opcjonalnych uprawnień umożliwiających wykonywanie odpowiednich akcji.'));
    steps.push(this.createStep('Ograniczenia konta', 'Dodatkowe ograniczenia nałożone na konto użytkownika w aplikacji.'));

    this.steps = steps;
  }

  private createStep(
    label: string, description: string) {

    const menu = { label, description };
    return menu;
  }

  private initGenders() {
    const genders: RoleModel[] = [];
    genders.push({ name: 'Kobieta', value: 'KOBIETA' });
    genders.push({ name: 'Mężczyzna', value: 'MEZCZYZNA' });
    genders.push({ name: 'Nieznane', value: 'NIEZNANE' });

    this.genders = genders;
  }

  private initRoles() {
    const roles: RoleModel[] = [];
    roles.push({ name: 'Administrator', value: 'ADMINISTRATOR' });
    roles.push({ name: 'Użytkownik zewnętrzny', value: 'UZYTKOWNIK_ZEWNETRZNY' });

    this.roles = roles;
  }

  private initPermissions() {
    const permissions: PermissionModel[] = [];
    permissions.push({ name: 'Zarządzanie kontami', value: 'ZARZADZANIE_KONTAMI' });
    permissions.push({ name: 'Zarządzanie uprawnieniami', value: 'ZARZADZANIE_UPRAWNIENIAMI' });
    permissions.push({ name: 'Raporty statystyczne', value: 'RAPORTY_STATYSTYCZNE' });

    this.permissions = permissions;
  }
}
