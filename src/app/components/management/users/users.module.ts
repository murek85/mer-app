import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { UsersService } from './users.service';

import { UsersComponent } from './users.component';
import { UsersRoutingModule } from './users-routing.module';

import { UserComponent } from './details/user.component';
import { UserFormComponent } from './form/form.component';

import { FiltersComponent } from './filters/filters.component';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot()
];

@NgModule({
  declarations: [
    UsersComponent,

    UserComponent,
    UserFormComponent,

    FiltersComponent
  ],
  imports: [
    UsersRoutingModule,

    APP_MODULES
  ],
  entryComponents: [],
  providers: [
    UsersService
  ]
})
export class UsersModule { }
