import { Component, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';


import { AppConfig } from 'src/app/app.config';

import { UsersService } from '../users.service';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { UserModel } from 'src/app/shared/models/user.model';

import * as moment from 'moment';
import { MatTabChangeEvent } from '@angular/material/tabs';

declare var $: any;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, AfterViewInit {

  userLogged: UserModel;

  user: UserModel;

  stats = [];

  aspId = null;

  configHeader = {
    header: 'Użytkownik',
    title: 'Szczegóły konta'
  };

  error: any;

  selectedIndex = 0;

  menus = [
      { label: 'Szczegóły konta', routerLink: '/management/users/' },
      { label: 'Bezpieczeństwo', routerLink: '/management/users/' },
      { label: 'Role i uprawnienia', routerLink: '/management/users/' },
      { label: 'Rejestr operacji', routerLink: '/management/users/' }
  ];

  activeMenu = null;

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private accountService: AccountService,
    private usersService: UsersService,
    private sidebarService: SidebarService) {

    this.aspId = this.route.snapshot.params['id'];

    this.menus.forEach(menu => menu.routerLink += `${this.aspId}`); this.activeMenu = this.menus[0];
  }

  public ngOnInit() {

    // ustawienie przycisku 'BACK'
    this.sidebarService.setSidebarMenuBack.next({
      show: false,
      routerLink: `/management/users`,
    });

    this.accountService.userLogged.subscribe(user => this.userLogged = user);
    this.accountService.getUserLogged();

    this.usersService.user.subscribe(user => {
        this.user = user;

        this.stats = [
            { name: 'poprawne logowanie', icon: 'clock outline', type: 'datetime',
              value: `${moment(this.user?.lastValidLogin).format('DD-MM-YYYY HH:mm:ss') }`},
            { name: 'nieudane logowanie', icon: 'clock outline', type: 'datetime',
              value: `${moment(this.user?.lastIncorrectLogin).format('DD-MM-YYYY HH:mm:ss') }`}
          ];
    });
    this.usersService.getUser(this.aspId);
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public changeTab($event) {
    const tab = $event as MatTabChangeEvent;
    switch (tab.index) {
      case 0:
        break;
      case 1:
        break;
      case 2:
        break;
      case 3:
        break;
      case 4:
        setTimeout(_ => {
          $('.checkbox-account-1').checkbox({
            onChecked: () => {
            },
            onUnchecked: () => {
            }
          });

          $('.checkbox-account-2').checkbox({
            onChecked: () => {
            },
            onUnchecked: () => {
            }
          });

          $('.checkbox-account-3').checkbox({
            onChecked: () => {
            },
            onUnchecked: () => {
            }
          });
        }, 0);
        break;
    }
  }

  private initSemanticConfig() {
    $('.tabs-profile .item').tab();

    $('.account-settings').dropdown({
      action: 'hide'
    });
  }

  public back() {
    this.router.navigate(['/management/users/list']);
    this.sidebarService.setSidebarMenuBack.next({ show: false, routerLink: null, queryParams: null });
  }

  public saveUser() { }
}
