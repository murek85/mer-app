import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { ReminderPasswordComponent } from './reminder-password.component';
import { ReminderPasswordRoutingModule } from './reminder-password-routing.module';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot()
];

@NgModule({
  declarations: [
    ReminderPasswordComponent
  ],
  imports: [
    ReminderPasswordRoutingModule,

    APP_MODULES
  ],
  providers: []
})
export class ReminderPasswordModule { }
