import { Component, OnInit, AfterViewInit } from '@angular/core';

import { AccountService } from 'src/app/core/services/account.service';

declare var $: any;

@Component({
  selector: 'app-reminder-password',
  templateUrl: './reminder-password.component.html',
  styleUrls: ['./reminder-password.component.scss']
})
export class ReminderPasswordComponent implements OnInit, AfterViewInit {

  email;

  constructor(
    public accountService: AccountService) { }

  private initSemanticConfig() { }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() { }

  public reminder() { }
}
