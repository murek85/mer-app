import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReminderPasswordComponent } from './reminder-password.component';

const routes: Routes = [
  { path: 'account/password/reminder',  component: ReminderPasswordComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ReminderPasswordRoutingModule { }
