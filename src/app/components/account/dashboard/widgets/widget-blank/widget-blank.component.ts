import { Component, OnInit, AfterViewInit, Input, Output } from '@angular/core';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';

declare var $: any;

@Component({
  selector: 'app-widget-blank',
  template: `
    <div style="padding: 0 1em;">
      <div class="ui basic segment"
        [ngClass]="{
          'teal bgc-grey-50': config.theme !== 'none'
        }"
        style="margin: 0; z-index: 1;"
        [ngStyle]="{
          'box-shadow': (config.theme !== 'none' ? '0px 0px 25px 5px rgba(0,0,0,0.1)' : 'none')
        }">

        <div class="ui basic segment" layout="row" layout-align="center center" style="padding: 0 0 .5em 0;">
          <div layout="column" layout-align="start start">
            <h4 class="ui header">
                <i *ngIf="config.icon" class="{{config.icon}} teal icon"></i>
                <div class="content">
                    <span>{{config.header | translate}}</span>
                </div>
            </h4>
          </div>

          <div layout="column" layout-align="start end" flex>
            <div layout="row">
              <div class="ui icon top right pointing dropdown circular button settings"
                style="box-shadow: 0px 0px 25px 5px rgba(0,0,0,0.1);">

                <i class="cog icon"></i>
                <div class="menu">
                  <div class="item">
                    <i class="pen icon"></i>
                    <span>{{'Edycja komponentu' | translate}}</span>
                  </div>
                  <div class="item">
                    <i class="trash alternate red icon"></i>
                    <span>{{'Usuń komponent' | translate}}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="ui basic segment"
        style="margin: 0; padding: 0;"
        [ngStyle]="{
          'box-shadow': (config.theme !== 'none' ? '0px 0px 25px 5px rgba(0,0,0,0.1)' : 'none')
        }">

        <ng-content select="[content]"></ng-content>
      </div>
      <div class="ui divider"
        style="padding-bottom: 1em; border-width: .2em;"
        [ngStyle]="{
          'margin-top': (config.theme === 'none' ? '1em' : '0')
        }"></div>
    </div>
  `,
  styles: [
    `.settings {
       background: transparent;
       box-shadow: 0px 0px 25px 5px rgba(0,0,0,0.1);
     }
     .settings:hover {
        background-color: rgba(128,203,196,1) !important;
        color: white !important;
      }
    }`
  ]
})
export class WidgetBlankComponent implements OnInit, AfterViewInit {

    @Input() config;

    constructor(
        private tdLoadingService: TdLoadingService,
        private translateService: TranslateService,
        private accountService: AccountService) { }

    private initSemanticConfig() {
      $('.settings').dropdown({
        action: 'hide'
      });
    }

    public ngAfterViewInit() {
        this.initSemanticConfig();
    }

    public ngOnInit() { }
}
