import { NgModule } from '@angular/core';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';

import { WidgetBlankComponent } from './widgets/widget-blank/widget-blank.component';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot()
];

@NgModule({
  declarations: [
    DashboardComponent,

    WidgetBlankComponent
  ],
  imports: [
    DashboardRoutingModule,

    APP_MODULES
  ],
  providers: []
})
export class DashboardModule { }
