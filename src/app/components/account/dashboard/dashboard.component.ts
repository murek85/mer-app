import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';

import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
  DAYS_OF_WEEK,
  CalendarDateFormatter
} from 'angular-calendar';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';

import { CustomDateFormatter } from './custom-date-formatter.provider';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { UserModel } from 'src/app/shared/models/user.model';

import * as moment from 'moment';

declare var $: any;

const COLORS: any = {
  default: {
    primary: '#78909C',
    secondary: '#90A4AE',
  }
};

const QUOTATIONS = [
  { title: 'Nigdy nie ma się drugiej okazji, żeby zrobić pierwsze wrażenie.', author: 'Andrzej Sapkowski' },
  { title: 'Przyjaciel to człowiek, który wie wszystko o tobie i wciąż cię lubi.', author: 'Elbert Hubbard' },
  { title: 'Nie każdy bliski jest bliski, nie każdy daleki – daleki.', author: 'Talmud' },
  { title: 'Nie jestem szalony, interesuje mnie wolność.', author: 'Jim Morrison' },
  { title: 'By dojść do źródła, trzeba płynąć pod prąd.', author: 'Stanisław Jerzy Lec' },
  { title: 'Poza tym, czy tego chcemy czy nie, zawsze na coś czekamy.', author: 'Lolita Pille' }
];

moment.updateLocale('pl', {
  week: {
    dow: DAYS_OF_WEEK.MONDAY,
    doy: 0
  }
});

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [
      {
          provide: CalendarDateFormatter,
          useClass: CustomDateFormatter
      }
  ]
})
export class DashboardComponent implements OnInit, AfterViewInit {

  userLogged: UserModel;

  system = { version: '1.0.0', date: '21-10-2019', type: 'dev', appName: 'Nazwa aplikacji' };
  languages: any[] = AppConfig.settings.i18n.availableLanguages;

  sidenav = false;

  date: Date = new Date();

  configCalendar = { header: 'Terminarz zdarzeń', icon: 'calendar alternate outline' };
  configHistoryOperation = { header: 'Rejestr zdarzeń', icon: 'history' };
  configMenu = { header: 'Co chcesz zrobić?', icon: 'question circle outline', theme: 'none' }
  configQuotation = { header: 'Cytat dnia', theme: 'none' }

  buttons = [
    { header: 'Utwórz konto', icon: 'user plus', routerLink: '/management/users/form' },
    { header: 'Zaplanuj spotkanie', icon: 'calendar plus outline' },
    { header: 'Nowe zdarzenie', icon: 'map marked alternate' },
    { header: 'Wyślij wiadomość', icon: 'comment outline' },
    { header: 'Historia operacji', icon: 'history' },
    { header: 'Dostosuj aplikację', icon: 'tools' },
    { header: 'Zmień hasło', icon: 'lock', routerLink: '/account/profile/security' },
    { header: 'Instrukcja obsługi', icon: 'book' }
  ];

  notifications = [
    { name: 'Dodano użytkownika', description: '', color: 'green', icon: 'user plus', date: new Date() },
    { name: 'Zaplanowano spotkanie', description: '', color: 'green', icon: 'calendar plus outline', date: new Date() },
    { name: 'Zaplanowano spotkanie', description: '', color: 'green', icon: 'calendar plus outline', date: new Date() },
    { name: 'Otrzymano wiadomość', description: '', color: 'green', icon: 'comments outline', date: new Date() },
    { name: 'Dodano użytkownika', description: '', color: 'green', icon: 'user plus', date: new Date() },
    { name: 'Zaplanowano spotkanie', description: '', color: 'green', icon: 'calendar plus outline', date: new Date() },
    { name: 'Otrzymano wiadomość', description: '', color: 'green', icon: 'comments outline', date: new Date() },
    { name: 'Otrzymano wiadomość', description: '', color: 'green', icon: 'comments outline', date: new Date() }
  ];

  view: CalendarView = CalendarView.Month;
  calendarView: typeof CalendarView = CalendarView;
  viewDate: Date = new Date();
  locale = 'pl';
  actions: CalendarEventAction[] = [];
  refresh: Subject<any> = new Subject();
  events: CalendarEvent[] = [
    {
      start: subDays(startOfDay(new Date()), 1),
      end: addDays(new Date(), 1),
      title: 'A 3 day event',
      color: COLORS.default,
      actions: this.actions,
      allDay: true
    },
    {
      start: startOfDay(new Date()),
      title: 'An event with no end date',
      color: COLORS.default,
      actions: this.actions,
    },
    {
      start: startOfDay(new Date()),
      title: 'An event with no end date',
      color: COLORS.default,
      actions: this.actions,
    },
    {
      start: subDays(endOfMonth(new Date()), 3),
      end: addDays(endOfMonth(new Date()), 3),
      title: 'A long event that spans 2 months',
      color: COLORS.default,
      allDay: true,
    },
    {
      start: addHours(startOfDay(new Date()), 2),
      end: addHours(new Date(), 2),
      title: 'A draggable and resizable event',
      color: COLORS.default,
      actions: this.actions
    },
  ];
  activeDayIsOpen = false;

  quotation = QUOTATIONS[this.getRandomIntInclusive(0, QUOTATIONS.length)];

  constructor(
    private router: Router,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private accountService: AccountService) { }

  private initSemanticConfig() {
    $('.notifications').dropdown({
      action: 'hide'
    });

    $('.settings').dropdown({
      action: 'hide'
    });
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);
    this.accountService.getUserLogged();

    this.system = {
      version: AppConfig.settings.system.version,
      date: AppConfig.settings.system.date,
      type: AppConfig.settings.system.type,
      appName: AppConfig.settings.system.appName
    };
  }

  getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  dayClicked({
    date, events
  }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventTimesChanged({
    event, newStart, newEnd,
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map((iEvent) => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd,
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
  }
}
