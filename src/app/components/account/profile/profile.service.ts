import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { TdLoadingService } from '@covalent/core/loading';

import { PagingSettingsModel, PagingResultsModel } from 'src/app/shared/models/paging.model';

@Injectable()
export class ProfileService {

  constructor(
    private tdLoadingService: TdLoadingService) { }

}
