import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ViewChildren, QueryList } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';
import { filter } from 'rxjs/operators';

import { AppConfig } from 'src/app/app.config';

import { ProfileService } from './profile.service';

import { FilterObjectModel } from 'src/app/shared/models/filter.model';
import { SortObjectModel } from 'src/app/shared/models/sort.model';
import { PagingSettingsModel, PagingResultsModel } from 'src/app/shared/models/paging.model';

import { AccountService } from 'src/app/core/services/account.service';

import { UserModel } from 'src/app/shared/models/user.model';

import * as moment from 'moment';
import { MatTab, MatTabChangeEvent } from '@angular/material/tabs';

declare var $: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, AfterViewInit {

  userLogged: UserModel;

  system = { version: '1.0.0', date: '21-10-2019', type: 'dev' };

  pagingSettings: PagingSettingsModel = new PagingSettingsModel();
  pagingResults: PagingResultsModel = new PagingResultsModel();

  selectedIndex = 0;

  stats = [];

  constructor(
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private accountService: AccountService) { }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => {
      this.userLogged = user;

      this.stats = [
        { name: 'poprawne logowanie', icon: 'clock outline', type: 'datetime',
          value: `${moment(this.userLogged?.lastValidLogin).format('DD-MM-YYYY HH:mm:ss') }`},
        { name: 'nieudane logowanie', icon: 'clock outline', type: 'datetime',
          value: `${moment(this.userLogged?.lastIncorrectLogin).format('DD-MM-YYYY HH:mm:ss') }`}
      ];
    });
    this.accountService.getUserLogged();

    this.route.params.pipe(filter(params => params.type))
      .subscribe(params => {
        switch (params.type) {
          case 'details':
            this.selectedIndex = 0;
            break;
          case 'security':
            this.selectedIndex = 1;
            break;
        }
      }
    );
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public changeTab($event) {
    const tab = $event as MatTabChangeEvent;
    switch (tab.index) {
      case 0:
        break;
      case 1:
        break;
      case 2:
        break;
      case 3:
        setTimeout(_ => {
          $('.checkbox-account-1').checkbox({
            onChecked: () => {
            },
            onUnchecked: () => {
            }
          });

          $('.checkbox-account-2').checkbox({
            onChecked: () => {
            },
            onUnchecked: () => {
            }
          });

          $('.checkbox-account-3').checkbox({
            onChecked: () => {
            },
            onUnchecked: () => {
            }
          });
        }, 0);
        break;
    }
  }

  private initSemanticConfig() {
    $('.tabs-profile .item').tab();
  }

  public saveUser() {
    this.tdLoadingService.register('listSyntax');

    $('body').toast({
      title: 'Pomyślnie zapisano',
      message: 'Poprawnie zapisano nowe informacje dotyczące Twojego konta użytkownika.',
      showProgress: 'bottom',
      showIcon: true,
      progressUp: true,
      classProgress: 'teal'
    });

    this.tdLoadingService.resolve('listSyntax');
  }
}
