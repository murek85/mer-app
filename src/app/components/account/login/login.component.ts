import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';

declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {

  userName;
  password;

  error;

  constructor(
    private router: Router,
    private tdLoadingService: TdLoadingService,
    public accountService: AccountService) { }

  private initSemanticConfig() { }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() { }

  public loginPasswordFlow() {
    this.error = null;

    if (!$('.ui.form-login').form('is valid')) {
      return;
    }

    this.tdLoadingService.register();
    this.accountService.loginPasswordFlow(this.userName, this.password)
    .then(result => {
      this.router.navigate(['dashboard']);
    })
    .catch(err => {
      console.error(err);
      this.error = err;
    }).finally(() => this.tdLoadingService.resolve());
  }

  public loginSocialCode(providerKey) {
    this.accountService.loginSocialCode(providerKey);
  }
}
