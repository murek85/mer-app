import { Component, OnInit, AfterViewInit } from '@angular/core';

import { AccountService } from 'src/app/core/services/account.service';

declare var $: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, AfterViewInit {

  userName;
  email;
  password;
  repeatPassword;

  constructor(
    public accountService: AccountService) { }

  private initSemanticConfig() { }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngOnInit() { }

  public register() { }
}
