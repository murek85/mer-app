import { NgModule, APP_INITIALIZER, Injectable } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { throwError, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthComponent } from './components/auth/auth.component';

import { AppConfig } from './app.config';

import { AccountService } from './core/services/account.service';
import { SidebarService } from './core/services/sidebar.service';

import { NgxMAuthOidcModule } from './core/services/oidc/oidc.module';
import { NgxMAuthOidcStorage } from './core/services/oidc/oidc-models';
import { NgxMAuthOidcService } from './core/services/oidc/oidc.service';

import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { LoginModule } from './components/account/login/login.module';
import { RegisterModule } from './components/account/register/register.module';
import { ReminderPasswordModule } from './components/account/reminder-password/reminder-password.module';
import { DashboardModule } from './components/account/dashboard/dashboard.module';
import { ProfileModule } from './components/account/profile/profile.module';
import { MapModule } from './components/map/map.module';
import { UsersModule } from './components/management/users/users.module';

const APP_MODULES: any[] = [
  CoreModule.forRoot(),
  SharedModule.forRoot(),
  LoginModule,
  RegisterModule,
  ReminderPasswordModule,
  DashboardModule,
  ProfileModule,
  MapModule,
  UsersModule
];

const APP_COMPONENTS: any[] = [
  AppComponent,
  AuthComponent
];

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(public oidcService: NgxMAuthOidcService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (!isNullOrUndefined(AppConfig.settings)) {
      const authorizationHeader = this.oidcService.authorizationHeader();
      if (!isNullOrUndefined(authorizationHeader)) {
        request = request.clone({
          // withCredentials: true,
          setHeaders: {
            Authorization: `${authorizationHeader}`
          }
        });
      }
    }

    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          // console.log('event', event);
        }

        return event;
      }),
      catchError((err: HttpErrorResponse) => {
        if (err instanceof ErrorEvent) {
          // client-side error
          console.debug(err);
        } else {
          // server-side error
          console.debug(err);
          switch (err.status) {
            case 400: {
              break;
            }
            case 401: {
              break;
            }
            case 404: {
              break;
            }
            case 500: {
              this.prepareError(err);
              break;
            }
          }
        }

        return throwError(err);
      })
    );
  }

  private prepareError(error: HttpErrorResponse) {

  }
}

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function initializeApp(appConfig: AppConfig) {
  return () => appConfig.load();
}

@NgModule({
  declarations: [
    APP_COMPONENTS
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (createTranslateLoader),
          deps: [HttpClient]
      }
    }),
    NgxMAuthOidcModule.forRoot(),
    AppRoutingModule,

    APP_MODULES
  ],
  providers: [
    AppConfig,
    { provide: APP_INITIALIZER, useFactory: initializeApp, deps: [AppConfig], multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: NgxMAuthOidcStorage, useValue: sessionStorage },

    AccountService,
    SidebarService,
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
