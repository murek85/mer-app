import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class SidebarService {
  setSidebarMenuBack: EventEmitter<any> = new EventEmitter<any>();
}
