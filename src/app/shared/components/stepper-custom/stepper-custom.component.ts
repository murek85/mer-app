import { Component, Input, Output } from '@angular/core';
import { CdkStepper } from '@angular/cdk/stepper';

@Component({
  selector: 'app-stepper-custom',
  templateUrl: './stepper-custom.component.html',
  styleUrls: ['./stepper-custom.component.scss'],
  // This custom stepper provides itself as CdkStepper so that it can be recognized
  // by other components.
  providers: [{ provide: CdkStepper, useExisting: StepperCustomComponent }]
})
export class StepperCustomComponent extends CdkStepper {

  @Input('stepsList') stepsList;

  @Input('error') error;

  onClick(index: number): void {
    this.selectedIndex = index;
  }
}
