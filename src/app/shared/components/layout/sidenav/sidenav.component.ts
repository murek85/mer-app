import { Component, OnInit, Inject, AfterViewInit, OnChanges, SimpleChanges } from '@angular/core';
import { TranslateService, DefaultLangChangeEvent } from '@ngx-translate/core';
import { Router, NavigationEnd } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';
import { SidebarService } from 'src/app/core/services/sidebar.service';

import { UserModel } from 'src/app/shared/models/user.model';

declare var $: any;

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit, AfterViewInit {

  menus: any[];
  selectedMenu: any;

  showQuestion = true;
  showSidebarMenuBack = false;
  routerLink = null;
  queryParams = null;

  userLogged: any;
  languages: any[] = AppConfig.settings.i18n.availableLanguages;

  constructor(
    private tdLoadingService: TdLoadingService,
    private router: Router,
    private accountService: AccountService,
    private sidebarService: SidebarService,
    public translateService: TranslateService) {

    this.menus = [];
  }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);

    this.translateService.onDefaultLangChange.subscribe(
      (params: DefaultLangChangeEvent) => {
        $('.languages').dropdown('set selected', params.lang);
      }
    );

    this.initMenus();

    this.sidebarService.setSidebarMenuBack.subscribe(result => {
      this.showSidebarMenuBack = result.show;
      this.routerLink = result.routerLink;
      this.queryParams = result.queryParams;
      this.showQuestion = result.question;
    });

    this.router.events.subscribe(navigation => {
      if (navigation instanceof NavigationEnd) {
        this.selectedMenu = this.menus.find(m => navigation.url.includes(m.routerLink));

        this.initSemanticConfig();
      }
    });
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public setSelectedMenu(menu: any) {
    this.selectedMenu = menu;
  }

  public initMenus() {
    this.menus.push(
      this.createMenu(
        'Dashboard', 'Dashboard',
        'mdi mdi-view-dashboard', 'tc-grey-50', 'dashboard', true, []
      ),
      this.createMenu(
        'Wiadomości', 'Wiadomości',
        'mdi mdi-inbox', 'tc-grey-50', 'messages', true, [], 12
      ),
      this.createMenu(
        'Mapa', 'Mapa',
        'mdi mdi-map', 'tc-grey-50', 'map', true, []
      ),
      this.createMenu(
        'Generowanie raportów', 'Generowanie raportów',
        'mdi mdi-text-box-multiple-outline', 'tc-grey-50', 'reports', true, []
      ),
      this.createMenu(
        'Zarządzanie aplikacją', 'Zarządzanie aplikacją',
        'mdi mdi-cogs', 'tc-grey-50', 'management/app/list', true, []
      ),
      this.createMenu(
        'Zarządzanie użytkownikami', 'Zarządzanie użytkownikami',
        'mdi mdi-account-multiple', 'tc-grey-50', 'management/users/list', true, []
      )
    );
  }

  public onLogout() {
    const dialog = $('.ui.modal.modal-logout');
    dialog.modal({
      closable: false,
      transition: 'fade',
      onDeny: () => {
        dialog.modal('hide');
      },
      onApprove: () => {
        dialog.modal('hide');
        this.accountService.logout();
      }
    }).modal('show');
  }

  public onBackPage() {
    const dialog = $('.ui.modal.modal-back-page');
    dialog.modal({
      closable  : false,
      transition: 'fade',
      onDeny: () => {
        dialog.modal('hide');
      },
      onApprove: () => {
        dialog.modal('hide');
        this.processBackPage();
      }
    }).modal('show');
  }

  private processBackPage() {
    this.router.navigate([this.routerLink], { queryParams: this.queryParams }).then(
      () => this.sidebarService.setSidebarMenuBack.next({
        show: false, routerLink: null, queryParams: null
      })
    );
  }

  private createMenu(
    name: string,
    toolTip: string,
    iconClass: string,
    iconColorClass: string,
    routerLink: string,
    selected: boolean,
    permissionsOnly?: string[],
    count?: number): any {

    const menu: any = {
      name,
      toolTip,
      iconClass,
      iconColorClass,
      routerLink,
      selected,
      permissionsOnly
    };

    menu.notifications = count ? { count } : null;

    return menu;
  }

  private initSemanticConfig() {
    setTimeout(_ => {
      $('.account').dropdown({
        on: 'click'
      });

      const language = JSON.parse(localStorage.getItem('language'));
      $('a.languages').dropdown({
        on: 'click',
        onChange: (value, text, $selectedItem) => {
          this.tdLoadingService.register();
          setTimeout(() => {
            localStorage.setItem('language', JSON.stringify(value));
            this.translateService.setDefaultLang(value);
            this.tdLoadingService.resolve();
          }, 500);
        }
      });

      $('.languages').dropdown('set selected',
        (language == null ? AppConfig.settings.i18n.defaultLanguage.code : language));
    }, 0);
  }
}
