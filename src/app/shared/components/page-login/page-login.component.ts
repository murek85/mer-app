import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';

declare var $: any;

@Component({
  selector: 'app-page-login',
  templateUrl: './page-login.component.html',
  styleUrls: ['./page-login.component.scss']
})
export class PageLoginComponent implements OnInit, AfterViewInit {

  system = { version: '1.0.0', date: '21-10-2019', type: 'dev' };
  languages: any[] = AppConfig.settings.i18n.availableLanguages;

  error;

  constructor(
    private router: Router,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    public accountService: AccountService) { }

  public ngOnInit() {
    this.system = {
      version: AppConfig.settings.system.version,
      date: AppConfig.settings.system.date,
      type: AppConfig.settings.system.type
    };
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  private initSemanticConfig() {
    const language = JSON.parse(localStorage.getItem('language'));
    $('.languages').dropdown({
      on: 'hover',
      onChange: (value, text, $selectedItem) => {
        this.tdLoadingService.register();
        setTimeout(() => {
          localStorage.setItem('language', JSON.stringify(value));
          this.translateService.setDefaultLang(value);
          this.tdLoadingService.resolve();
        }, 500);
      }
    });

    $('.languages').dropdown('set selected',
      (language == null ? AppConfig.settings.i18n.defaultLanguage.code : language));

  }

  public regulations() {
    $('.ui.modal.modal-regulations')
      .modal({
        context: 'td-layout',
        transition: 'fade',
      })
      .modal('show');
  }

  public privacy() {
    $('.ui.modal.modal-privacy')
      .modal({
        context: 'td-layout',
        transition: 'fade',
      })
      .modal('show');
  }

  public cookie() {
    $('.ui.modal.modal-cookie')
      .modal({
        context: 'td-layout',
        transition: 'fade',
      })
      .modal('show');
  }
}
