import { CommonModule, registerLocaleData } from '@angular/common';
import { NgModule, ModuleWithProviders, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule, JsonpModule } from '@angular/http';

import { TranslateModule } from '@ngx-translate/core';

// Covalent
import { CovalentCommonModule } from '@covalent/core/common';
import { CovalentMenuModule } from '@covalent/core/menu';
import { CovalentLayoutModule } from '@covalent/core/layout';
import { CovalentNotificationsModule } from '@covalent/core/notifications';
import { CovalentMediaModule } from '@covalent/core/media';
import { CovalentStepsModule } from '@covalent/core/steps';
import { CovalentChipsModule } from '@covalent/core/chips';
import { CovalentPagingModule } from '@covalent/core/paging';
import { CovalentDataTableModule } from '@covalent/core/data-table';
import { CovalentExpansionPanelModule } from '@covalent/core/expansion-panel';
import { CovalentMessageModule } from '@covalent/core/message';
import { CovalentSearchModule } from '@covalent/core/search';
import { CovalentDialogsModule } from '@covalent/core/dialogs';
import { CovalentLoadingModule } from '@covalent/core/loading';
import { CovalentJsonFormatterModule } from '@covalent/core/json-formatter';
import { CovalentFileModule } from '@covalent/core/file';
// (optional) Additional Covalent Modules imports

// Angular Material
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatMomentDateModule, MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';

import { MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material/core';

// Components
import { SidenavComponent } from './components/layout/sidenav/sidenav.component';
import { PageLoginComponent } from './components/page-login/page-login.component';
import { ListComponent } from './components/table/list/list.component';
import { FiltersComponent } from './components/table/filters/filters.component';
import { StepperCustomComponent } from './components/stepper-custom/stepper-custom.component';

// Other 3rd components
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { AvatarModule } from 'ngx-avatar';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { FlexLayoutModule } from '@angular/flex-layout';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgxPermissionsModule } from 'ngx-permissions';
import { NgxEchartsModule } from 'ngx-echarts';

import localePl from '@angular/common/locales/pl';

const ANGULAR_MODULES: any[] = [
  CommonModule,
  RouterModule,
  FormsModule,
  ReactiveFormsModule,
  HttpModule,
  JsonpModule,
  TranslateModule
];

const MATERIAL_MODULES: any[] = [
  MatAutocompleteModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  MatRadioModule,
  MatSelectModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatMenuModule,
  MatSidenavModule,
  MatToolbarModule,
  MatListModule,
  MatGridListModule,
  MatCardModule,
  MatStepperModule,
  MatTabsModule,
  MatExpansionModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatChipsModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatDialogModule,
  MatTooltipModule,
  MatSnackBarModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatMomentDateModule,

  CdkStepperModule,
  DragDropModule,
  ScrollingModule
];

const COVALENT_MODULES: any[] = [
  CovalentCommonModule,
  CovalentMenuModule,
  CovalentLayoutModule,
  CovalentNotificationsModule,
  CovalentMediaModule,
  CovalentStepsModule,
  CovalentChipsModule,
  CovalentPagingModule,
  CovalentDataTableModule,
  CovalentExpansionPanelModule,
  CovalentMessageModule,
  CovalentSearchModule,
  CovalentDialogsModule,
  CovalentLoadingModule,
  CovalentJsonFormatterModule,
  CovalentFileModule
];

const BOOTSTRAP_MODULES: any[] = [];

const CUSTOM_MODULES: any[] = [
  FlexLayoutModule,
  InfiniteScrollModule,
  NgxEchartsModule
];

const COMPONENTS: any[] = [
  SidenavComponent,
  PageLoginComponent,
  StepperCustomComponent,
  ListComponent,
  FiltersComponent
];

const DIALOGS: any[] = [];

const PIPES: any[] = [];

const DIRECTIVES: any[] = [];

registerLocaleData(localePl);

@NgModule({
  imports: [
    ANGULAR_MODULES,
    MATERIAL_MODULES,
    COVALENT_MODULES,
    BOOTSTRAP_MODULES,
    CUSTOM_MODULES,

    AvatarModule.forRoot({
      colors: ['#4DB6AC']
    }),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    NgxPermissionsModule.forRoot({
      permissionsIsolate: true
    })
  ],
  declarations: [
    COMPONENTS,
    DIALOGS,
    PIPES,
    DIRECTIVES
  ],
  exports: [
    ANGULAR_MODULES,
    MATERIAL_MODULES,
    COVALENT_MODULES,
    BOOTSTRAP_MODULES,
    CUSTOM_MODULES,
    COMPONENTS,
    DIALOGS,
    PIPES,
    DIRECTIVES,

    AvatarModule,
    CalendarModule,
    NgxPermissionsModule
  ],
  entryComponents: [

  ],
  providers: [
    // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
    // `MatMomentDateModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    // { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    // { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    // { provide: DateAdapter, useClass: NativeDateAdapter },
    // { provide: MAT_DATE_FORMATS, useValue: MAT_NATIVE_DATE_FORMATS },
    // { provide: MAT_DATE_LOCALE, useValue: 'pl-PL' },
    // { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } }
    { provide: MAT_DATE_LOCALE, useValue: 'pl-PL' },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    // { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] }
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class SharedModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: []
    };
  }
}
