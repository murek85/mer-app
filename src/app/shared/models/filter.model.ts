export class FilterObjectModel<T> {
  name: string;
  tooltip?: string | null;
  desc?: string | null;
  type: T;
  field?: string | null;
  value?: any | null;
  objectsModel?: any | null;
  active?: boolean;
}
