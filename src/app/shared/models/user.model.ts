import { RoleModel } from './role.model';
import { PermissionModel } from './permission.model';

export class UserModel {
  id?: string;
  userName?: string;
  password?: string;
  passwordConfirm?: string;
  passwordNew?: string;
  email?: string;
  firstName?: string;
  lastName?: string;
  fullName?: string;
  peselNumber?: number;
  phoneNumber?: string;
  lastValidLogin?: Date;
  lastIncorrectLogin?: Date;
  roles?: RoleModel[];
  permissions?: PermissionModel[];
  logins?: string[];
  provider?: any;
}
