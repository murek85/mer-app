export class PagingSettingsModel {
  pageNumber = 1;
  pageSize = 5;
}

export class PagingResultsModel {
  pageNumber = 1;
  pageSize = 5;
  totalRows = 0;
}
