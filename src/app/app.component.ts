import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService, DefaultLangChangeEvent } from '@ngx-translate/core';

import { AppConfig } from './app.config';
import { authConfig } from './auth.config';

import { UserModel } from './shared/models/user.model';

import { NgxMAuthOidcService } from './core/services/oidc/oidc.service';
import { AccountService } from './core/services/account.service';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {

  userLogged: UserModel;
  languages: any[] = AppConfig.settings.i18n.availableLanguages;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    private oidcService: NgxMAuthOidcService,
    public accountService: AccountService) { }

  public ngOnInit() {
    this.accountService.userLogged.subscribe(user => this.userLogged = user);

    this.translateService.onDefaultLangChange.subscribe(
      (params: DefaultLangChangeEvent) => {
        $('.languages').dropdown('set selected', params.lang);
      }
    );

    this.oidcService.configure(authConfig(AppConfig.settings.oidc.apiUrl));
    this.oidcService.setStorage(sessionStorage);
    this.oidcService.loadDocumentAndTryLogin({ customHashFragment: '?' })
    .then(res => {
      if (this.accountService.getLoggedIn()) {
        this.accountService.getUserLogged();
      }
    })
    .catch(err => {
      console.error('loadDocumentAndTryLogin error', err);
    });

    this.oidcService.events.subscribe(e => {
      switch (e.type) {
        case 'token_received': {
          this.accountService.getUserLogged();
          break;
        }
        case 'token_expires': {
          this.oidcService.scope = AppConfig.settings.oidc.scope;
          this.oidcService.refreshToken()
              .then()
              .catch(err => {
                console.error(e, err);
              });
          break;
        }
        case 'token_error':
        case 'token_refresh_error': {
          this.accountService.logout();
          this.router.navigate(['/login']);
          break;
        }

        case 'logout': {
          this.router.navigate(['/login']);
          break;
        }
      }
    });
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  private initSemanticConfig() {
  }
}
